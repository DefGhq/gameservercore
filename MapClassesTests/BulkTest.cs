﻿using System.Numerics;
using MapClasses;
using Xunit;

namespace MapClassesTests
{
    public class BulkTest
    {
        [Fact]
        public void TestInitialisation()
        {
            Tile someTile = new Tile(Vector2.One); 
            
            Assert.NotNull(someTile);
            Assert.Equal(someTile.Index, Vector2.One);
        }

        [Fact]
        public void TestUpdate()
        {
            Tile someTile = new Tile(Vector2.One);
            someTile.Update();
        }
    }
}