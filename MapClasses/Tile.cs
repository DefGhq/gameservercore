﻿using System.Collections.Generic;
using System.Numerics;
using GameObjects;

namespace MapClasses
{
    public class Tile
    {
        public readonly Vector2 Index;

        public List<GameObject> GameObjects;
        
        public Tile(Vector2 index)
        {
            Index = index;
        }

        public void Update()
        {
            
        }
    }
}