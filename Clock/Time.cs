﻿using System;

namespace Clock
{
    public static class Time
    {
        public static DateTime Now;

        private static DateTime LastTime;

        public static void Init()
        {
            Now = DateTime.Now;
        }

        public static void Update()
        {
            LastTime = Now;
            Now = DateTime.Now;
            DeltaTime = (LastTime - Now).TotalMilliseconds;
        } 

        public static double DeltaTime;
    }
}