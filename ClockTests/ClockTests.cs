﻿using System;
using Clock;
using Xunit;

namespace ClockTests
{
    public class ClockTests
    {
        [Fact]
        public void TestClockUpdate()
        {
            Time.Init();
            Time.Update();
            
            Console.WriteLine(Time.DeltaTime);
            Assert.NotEqual(Time.DeltaTime, 0);
        }
    }
}