﻿using System;
using System.Numerics;

namespace GameObjects
{
    public class GameObject
    {
        public bool Active;
        public Vector2 Position;
        public Vector2 TargetPosition;

        public float Speed;

        public void Update()
        {
            Position = Position + (TargetPosition - Position) / (TargetPosition - Position).Length() * Speed;
        }

    }
    
    
}
